﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityStandardAssets._2D;

public class Billy : MonoBehaviour {

	public AudioSource MudSound, PaintCanSound;

	private GameObject _emmy;
	private GameObject _camera;
	//public Animator Anim; 
	public List<AudioSource> BillySounds;

	public GameObject PaintCover1, PaintCover2, MudCover;
	public GameObject BillyAngry;

	void Start(){
		//Anim = GetComponent<Animator>();
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player");
		_camera = GameObject.FindGameObjectWithTag("MainCamera");
	}

	void Update(){
		if(_emmy.GetComponent<PlayerController>().SolvedPuzzleCount == 3){
			BillyAngry.SetActive(false);
			gameObject.GetComponent<Renderer>().enabled = true;
		}else if(_emmy.transform.position.x > 95){
			BillyAngry.SetActive(true);
			gameObject.GetComponent<Renderer>().enabled = false;
		}
	}

	void OnMouseDown(){
		if(_emmy.GetComponent<PlayerController>().HasMud){
			_emmy.GetComponent<PlayerController>().animator.SetTrigger("Throw");
			Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(x => {
				MudSound.Play();
				BillySounds[2].Play();
				Observable.Timer(TimeSpan.FromSeconds(2)).Subscribe(y => {
					BillySounds[2].Stop();}
				);
				//Color myColor = new Color(121.0f/255, 76.0f/255, 19.0f/255);
				//gameObject.GetComponent<SpriteRenderer>().color = myColor;
				_emmy.GetComponent<PlayerController>().SolvedPuzzleCount++;
				MudCover.SetActive(true);
				_emmy.GetComponent<PlayerController>().HasMud = false;
			});
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "PaintCan"){
			//Color myColor = new Color(21.0f/255, 26.0f/255, 119.0f/255);
			//gameObject.GetComponent<SpriteRenderer>().color = myColor;
			_camera.GetComponent<Camera2DFollow>().target = transform;

			PaintCover1.SetActive(true);
			PaintCanSound.Play();
			_emmy.GetComponent<PlayerController>().SolvedPuzzleCount++;
			Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(x => {
				BillySounds[0].Play();
				_camera.GetComponent<Camera2DFollow>().target = _emmy.transform;
				//_emmy.GetComponent<PlayerController>().NextLevelCanvas.gameObject.SetActive(true);
			
				//_emmy.GetComponent<PlayerController>().InventoryText.text = "Inventory: ";
			});
		}
		else if(col.gameObject.tag == "WhiteCan"){
			//Color myColor = new Color(21.0f/255, 26.0f/255, 119.0f/255);
			//gameObject.GetComponent<SpriteRenderer>().color = myColor;
			_camera.GetComponent<Camera2DFollow>().target = transform;
			PaintCanSound.Play();
			PaintCover2.SetActive(true);
			_emmy.GetComponent<PlayerController>().SolvedPuzzleCount++;
			Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(x => {
				BillySounds[1].Play();
				_camera.GetComponent<Camera2DFollow>().target = _emmy.transform;
				//_emmy.GetComponent<PlayerController>().NextLevelCanvas.gameObject.SetActive(true);

				//_emmy.GetComponent<PlayerController>().InventoryText.text = "Inventory: ";
			});

		
		}

	}

	public void OnNextLevelButtonPressed(){
		Application.LoadLevel ("Level_2");
	}
}
