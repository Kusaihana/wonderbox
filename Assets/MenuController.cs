﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {

	public GameObject ControlMenu;
	public GameObject CreditsMenu;
	public GameObject MenuPanel;

	public AudioSource clicksound;

	GameObject _emmy;

	public void OnContinueButtonPressed(){
		clicksound.Play();
		MenuPanel.SetActive(false);
		_emmy.GetComponent<PlayerController>().UnFreezeMotion();
	}

	void Start(){
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player");
	}

	public void OnNewGameButtonPressed(){
		clicksound.Play();
		Application.LoadLevel ("Level_0");
	}

	public void OnControlsButtonPressed(){
		clicksound.Play();
		CreditsMenu.SetActive(false);
		ControlMenu.SetActive(true);
	}

	public void OnCreditsButtonPressed(){
		clicksound.Play();
		ControlMenu.SetActive(false);
		CreditsMenu.SetActive(true);
	}

	public void OnQuitButtonPressed(){
		clicksound.Play();
		Application.Quit();
	}
}
