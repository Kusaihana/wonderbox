﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchoolBag : MonoBehaviour {

	public GameObject WaterBottle;
	private bool _isGrounded;
	private bool _hasBottle = true;


	void Update () {
		if(transform.position.y < 0)
			_isGrounded =  true;

		if(_isGrounded && _hasBottle){
			// play sound
			var hitAudio = gameObject.GetComponent<AudioSource>();
			hitAudio.Play();
			GameObject go = (GameObject)Instantiate(WaterBottle, transform.position + new Vector3(2,0,0), Quaternion.identity);
			_hasBottle = false;
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.tag == "Player"){
			gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
		}
	}
}
