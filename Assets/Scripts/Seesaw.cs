﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class Seesaw : MonoBehaviour {

	public GameObject CanSlot;
	public GameObject JumpSlot;

	private GameObject _emmy;
	private GameObject _can;
	private GameObject _camera;
	private GameObject _billy;
	private int _clickCount = 0;
	private bool _throw = false;

	void Start(){
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player");
		_billy = GameObject.FindGameObjectWithTag("Boss");
		_camera = GameObject.FindGameObjectWithTag("MainCamera");
	}

	void Update(){
		if(_throw){
			_camera.GetComponent<Camera2DFollow>().target = _can.transform;
			float cTime = Time.time * 0.0019f;
			var startPos = _can.transform.position+ new Vector3(0,1,0);
			var endPos = _billy.transform.position + new Vector3(0,10,0);
			// calculate straight-line lerp position:
			Vector3 currentPos = Vector3.Lerp(startPos, endPos, cTime);
			// add a value to Y, using Sine to give a curved trajectory in the Y direction
			currentPos.y += 10 * Mathf.Sin(Mathf.Clamp01(cTime) * Mathf.PI);
			// finally assign the computed position to our gameObject:
			_can.transform.position = currentPos;
		}
		if(_can != null && _billy.transform.position.x - _can.transform.position.x < 5){
			_can.GetComponent<BoxCollider2D>().enabled = true;
			_throw = false;
		}
	}

	void OnMouseDown(){
		if(Mathf.Abs(_emmy.transform.position.x - transform.position.x) < 20){
			var pc = _emmy.GetComponent<PlayerController>();

			if(pc.PaintCan){
				_can = pc.PaintCan;
				pc.PaintCan.transform.position = CanSlot.transform.position;
				pc.PaintCan.SetActive(true);
				if(pc.HasPaintCan){
					pc.InventoryText.text = "Inventory:";
					if(pc.HasMud){
						pc.InventoryText.text += "\nMud";
					}
					if(pc.HasWaterBottle){
						pc.InventoryText.text += "\nWater Bottle";
					}
					_clickCount = 1;
					pc.HasPaintCan = false;
					pc.PaintCan = null;
				}
			}else if(_clickCount == 1){
				_emmy.transform.position = JumpSlot.transform.position;
				_throw = true;
				_can.GetComponent<BoxCollider2D>().enabled = false;
				_can.GetComponent<Rigidbody2D>().gravityScale = 1;



			}
		}
	}
}
