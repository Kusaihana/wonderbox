﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class Window : MonoBehaviour {

	private GameObject _emmy;

	public GameObject BrokenWindow;

	public AudioSource brokenSound;

	void Start(){
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player");
	}
	
	void OnMouseDown(){
		if(Mathf.Abs(_emmy.transform.position.x - transform.position.x) < 50){
			var pc = _emmy.GetComponent<PlayerController>();
			pc.animator.SetTrigger("Throw");
			if(_emmy.GetComponent<PlayerController>().HasRock){
				brokenSound.Play();
				Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe(x => {
					BrokenWindow.SetActive(true);
					gameObject.GetComponent<BoxCollider2D>().enabled = false;
					_emmy.GetComponent<PlayerController>().HasRock = false;
					_emmy.GetComponent<PlayerController>().InventoryText.text = "Inventory:";
				});

			}
		}
	}
}
