﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scare : MonoBehaviour {

	public bool destroyNonPlayerObjects = true;
	public bool PuzzleSolved = false;
	public bool IsBigBoss = false;
	public bool IsFirstGap = false;
	public bool IsSecondGap = false;

	public AudioSource screamsound;

	private int _coverCount;

	void Awake(){
		var audios = gameObject.GetComponents<AudioSource>();
		screamsound = audios[0]; 
	}

	void OnTriggerEnter2D (Collider2D other) {
		if(IsFirstGap){
			if (other.gameObject.tag == "Player")
			{
				screamsound.Play();
				other.gameObject.transform.position = new Vector3(-595,3);
			}
		}
		else if(IsSecondGap){
			if (other.gameObject.tag == "Player")
			{
				screamsound.Play();
				other.gameObject.transform.position = new Vector3(-266,3);
			}
		}
		else if(!IsBigBoss && !PuzzleSolved){
			if (other.gameObject.tag == "Player")
			{
				screamsound.Play();
				other.gameObject.transform.position = new Vector3(-148,37);
				//other.gameObject.GetComponent<PlayerController>().FallDeath ();
			}
		}
		else if(IsBigBoss){
			if (other.gameObject.tag == "Player")
			{
				_coverCount = other.gameObject.GetComponent<PlayerController>().SolvedPuzzleCount;
				if(_coverCount != 3){
					screamsound.Play();
					other.gameObject.transform.position = new Vector3(-11,3);
				}
			}
		}
	}
}
