﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UniRx;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	[HideInInspector] public bool jump = false;
	public float moveForce = 80f;
	public float elapsed;
	public float maxSpeed = 5f;
	public float jumpForce = 100f;
	public Transform groundCheck;
	public LayerMask whatIsGround;
	public bool canClimb = false;

	[HideInInspector]
	public bool playerCanMove = true;

	public bool grounded = false;
	public bool OnAir = false;
	private Rigidbody2D rb2d;

	public AudioSource footsound;
	public AudioSource jumpsound;
	public AudioSource climbsound;
	public AudioSource DangerSound;

	public GameObject Bubble;
	public GameObject NextLevelCanvas;
	public GameObject IngameMenu;
	public Scare Billy;

	public Text InventoryText;

	public int SolvedPuzzleCount = 0;

	public Animator animator; 

	//InventoryRelatedStuffGoesHere!
	public bool HasWaterBottle = false;
	public bool HasMud = false;
	public bool HasPaintCan = false;
	public bool HasRock = false;
	public bool HasJam = false;
	public GameObject PaintCan;

	public int LevelNumber;

	void Awake () 
	{
		rb2d = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
		rb2d.freezeRotation = true;
		var factory = gameObject.GetComponents<AudioSource>();
		footsound = factory[0]; 
		jumpsound = factory[1]; 
	}

	void Update () 
	{

		if (playerCanMove) {
			//grounded = Physics2D.Linecast(transform.position, groundCheck.position, whatIsGround);  

			if (Input.GetButtonDown ("Jump") && grounded && !OnAir) {
				OnAir = true;
				jump = true;
				footsound.Stop();
				jumpsound.Play();
				animator.SetTrigger ("Jump");
				grounded = false;
			}
		}

		if(rb2d.velocity.y < 0 && !grounded){
			grounded = false;
			animator.SetBool("IsFalling", true);
		}else{
			animator.SetBool("IsFalling", false);
		}

		if (Math.Abs(rb2d.velocity.x) > 0 && grounded && !OnAir){
			if(!footsound.isPlaying){
				footsound.Play();
				footsound.loop = true;
				animator.SetBool("IsWalking",true);
			}
		}else{
			footsound.Stop();
			animator.SetBool("IsWalking",false);
			animator.SetBool("IsPushing",false);
		}

		if(canClimb){
			if(Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)){
				rb2d.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
				animator.speed = 1;
				animator.SetBool("IsClimbing",true);
				climbsound.Play();
			}
			if(Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)){
				if(!grounded){
					OnAir = true;
					climbsound.Play();
					rb2d.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
					animator.speed = 1;
					animator.SetBool("IsClimbing",true);
				}else{
					climbsound.Stop();
					playerCanMove = true;
					rb2d.gravityScale = 1;
					rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
					animator.speed = 1;
					animator.SetBool("IsClimbing",false);
				}

			}

			float h = Input.GetAxis ("Vertical");
			animator.SetBool("IsClimbing",true);
			transform.Translate (new Vector2(0,h) * Time.deltaTime*maxSpeed);
			if(h == 0 && !animator.GetBool("IsPushing")){
				climbsound.Stop();
				animator.speed = 0;
			} 
			rb2d.gravityScale = 0;
			/*float y = Input.GetAxis ("Vertical");
			Debug.Log(y);
			transform.Translate(new Vector3 (0, y * 0.10f, 0));*/
		}else{
			playerCanMove = true;
			rb2d.gravityScale = 1;
			rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
			if(climbsound != null)climbsound.Stop();
			animator.speed = 1;
			animator.SetBool("IsClimbing",false);
		}
	}

	void FixedUpdate()
	{
		if (playerCanMove) {
			float h = Input.GetAxis ("Horizontal");
			if(h == 0){
				rb2d.velocity = new Vector2(0, rb2d.velocity.y);
			}

			//for animation fix
			if(rb2d.velocity.x < -0.1f){
				Vector3 theScale = transform.localScale;
				theScale.x = -1;
				transform.localScale = theScale;
			}else if(grounded){
				Vector3 theScale = transform.localScale;
				theScale.x = 1;
				transform.localScale = theScale;
			}
			if(transform.localScale.x == -1 && !grounded){
				Vector3 theScale = transform.localScale;
				theScale.x = -1;
				transform.localScale = theScale;
			}

			if (h * rb2d.velocity.x < maxSpeed){
				rb2d.AddForce (Vector2.right * h * moveForce);
			}
				

			if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
				rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

			if (jump) {
				rb2d.AddForce (new Vector2 (0f, jumpForce));
				jump = false;
			}
		}
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			IngameMenu.SetActive(true);
			FreezeMotion();
		}

		if(SolvedPuzzleCount == 3){
			NextLevelCanvas.gameObject.SetActive(true);
			Billy.enabled = false;
			DangerSound.Stop();
		}
	}

	public void FreezeMotion() {
		playerCanMove = false;
		rb2d.isKinematic = true;
	}

	public void UnFreezeMotion() {
		playerCanMove = true;
		rb2d.isKinematic = false;
	}

	public void ApplyDamage () {

		StartCoroutine (KillPlayer ());

	}

	public void FallDeath () {
		if (playerCanMove) {
			StartCoroutine (KillPlayer ());
		}
	}

	IEnumerator KillPlayer()
	{
		FreezeMotion();
		yield return new WaitForSeconds(1.0f);
		LevelNumber = 0;
		Application.LoadLevel ("Level_1");
	}

	public void Respawn(Vector3 spawnloc) {
		UnFreezeMotion();
		groundCheck.parent = null;
		groundCheck.position = spawnloc;

	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.layer == LayerMask.NameToLayer("Ground"))
		{
			grounded = true;
			OnAir = false;
		}else if(col.gameObject.tag == "EndOfLevel"){
			//NextLevelCanvas.gameObject.SetActive(true);
			if(LevelNumber == 0) Application.LoadLevel("Level_0_3");
			else if(LevelNumber == 1) Application.LoadLevel("Level_0_3");
			else if(LevelNumber == 2) Application.LoadLevel("Level_1");
			LevelNumber++;
		}/*else if(col.gameObject.tag == "PrevLevel"){
			if(LevelNumber == 1) Application.LoadLevel("Level_0");
			else if(LevelNumber == 2) Application.LoadLevel("Level_0_2");
			else if(LevelNumber == 3) Application.LoadLevel("Level_0_3");
			LevelNumber++;
		}*/else if(((col.gameObject.tag == "Box" && Math.Abs(rb2d.velocity.x) - 1> 0) ||
			(col.gameObject.tag == "PaintCan" && Math.Abs(rb2d.velocity.x) - 1 > 0) ||
			(col.gameObject.tag == "WhiteCan" && Math.Abs(rb2d.velocity.x)> 0))&& !animator.GetBool("IsPushing")){
			animator.speed = 1;
			animator.SetBool("IsPushing",true);
		}else if(col.gameObject.tag == "Lamp"){
			animator.speed = 1;
			animator.SetBool("IsPushing", true);
			Vector3 theScale = transform.localScale;
			theScale.x = 1;
			transform.localScale = theScale;
		}
	}
		
	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "FlashbackItem"){
			var pos = transform.position;
			GameObject go = (GameObject)Instantiate(Bubble, new Vector3(pos.x+15,8,pos.z), Quaternion.identity);
			go.GetComponent<SpriteRenderer>().sprite = col.gameObject.GetComponent<Flashback>().FlashbackSprite;
			rb2d.velocity = Vector3.zero;
			FreezeMotion();

			// play sound
			var flashbackAudio = col.gameObject.GetComponent<AudioSource>();
			flashbackAudio.Play();

			Observable.Timer(TimeSpan.FromSeconds(col.gameObject.GetComponent<Flashback>().FlashBackDuration)).Subscribe(x => {
				Destroy(go);
				playerCanMove = true;
				flashbackAudio.Stop();
				UnFreezeMotion();
				if(col.gameObject.GetComponent<Flashback>().PlayOnce) Destroy(col.gameObject);
			});
		}
	}

	public void OnNextLevelButtonPressed(){
		Application.LoadLevel ("Level_1");
	}
}