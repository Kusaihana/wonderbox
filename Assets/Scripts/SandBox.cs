﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandBox : MonoBehaviour {

	private GameObject _emmy;
	private bool _isMud = false;

	public AudioSource pickupSound;

	void Start(){
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player");
	}

	void OnMouseDown(){
		if(Mathf.Abs(_emmy.transform.position.x - transform.position.x) < 20){
			var pc = _emmy.GetComponent<PlayerController>();
			if(_isMud && !pc.HasMud){
				pickupSound.Play();
				pc.HasMud = true;
			}
			// Change texture
			if(_emmy.GetComponent<PlayerController>().HasWaterBottle){
				pickupSound.Play();
				Color myColor = new Color(121.0f/255, 76.0f/255, 19.0f/255);
				gameObject.GetComponent<SpriteRenderer>().color = myColor;
				_emmy.GetComponent<PlayerController>().HasWaterBottle = false;
				_isMud = true;
			}
			Debug.Log(_emmy.GetComponent<PlayerController>().HasMud);
		}
	}
}
