﻿using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
	public class Camera2DFollow : MonoBehaviour
	{
		public Transform target;
		public float damping = 1;
		public float lookAheadFactor = 3;
		public float lookAheadReturnSpeed = 0.5f;
		public float lookAheadMoveThreshold = 0.1f;
		public bool IsIntro;
		public bool IsIntro3;

		float m_OffsetZ;
		Vector3 m_LastTargetPosition;
		Vector3 m_CurrentVelocity;
		Vector3 m_LookAheadPos;

		private void Start()
		{
			m_LastTargetPosition = target.position;
			m_OffsetZ = (transform.position - target.position).z;
			transform.parent = null;

			if (target==null) {
				target = GameObject.FindGameObjectWithTag("Player").transform;
			}
			
			if (target==null)
				Debug.LogError("Target not set on Camera2DFollow.");
			
		}

		private void Update()
		{
			if (target == null)
				return;

			float xMoveDelta = (target.position - m_LastTargetPosition).x;
			
			bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;
			
			if (updateLookAheadTarget)
			{
				m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
			}
			else
			{
				m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
			}
			
			Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ;
			var shiftAmount = new Vector3(0,20f,0);
			if(IsIntro) shiftAmount = new Vector3(0,10f,0);
			Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos + shiftAmount, ref m_CurrentVelocity, damping);

			transform.position = newPos;

			var maxy = 45;

			if(target.position.x > -15) maxy = 24;

			var boundLeft = -575;
			var boundRight = 112;
			if(IsIntro){
				boundLeft = -5;
				boundRight = 65;
			}else if(IsIntro3){
				boundLeft = 32;
				boundRight = 89;
			}

			transform.position = new Vector3(Mathf.Clamp(newPos.x,boundLeft,boundRight),Mathf.Clamp(newPos.y,0,maxy),newPos.z);

			m_LastTargetPosition = target.position;
		}
	}
}
