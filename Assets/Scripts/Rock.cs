﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class Rock : MonoBehaviour {

	private GameObject _emmy;
	public AudioSource pickupSound;

	void Start(){
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player");
	}

	void OnMouseDown(){
		if(Mathf.Abs(_emmy.transform.position.x - transform.position.x) < 10){
			pickupSound.Play();
			Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe(x => {
				Destroy(gameObject);
				_emmy.GetComponent<PlayerController>().HasRock = true;
			});
		}
	}
}
