﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class Milk : MonoBehaviour {

	public GameObject Dog;
	public AudioSource SplashSound;

	public GameObject MilkFlat, MilkSplash;

	private GameObject _emmy;
	private bool _isCovered;

	void Start(){
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player");
	}

	void OnMouseDown(){
		if(Mathf.Abs(_emmy.transform.position.x - transform.position.x) < 10 && !_isCovered){
			SplashSound.Play();
			gameObject.GetComponent<Renderer>().enabled = false;
			MilkFlat.SetActive(true);
			MilkSplash.SetActive(true);
			var dog = Dog.GetComponent<Dog>();

			_isCovered = true;
			Observable.Timer(TimeSpan.FromSeconds(0.6f)).Subscribe(x => {
				MilkSplash.SetActive(false);
				dog.DogSounds[1].Play();
				dog.PaintCover2.SetActive(true);
				dog.CoverCount++;
				if(dog.CoverCount == 2){
					dog.GetComponent<Scare>().PuzzleSolved = true;
					dog.DangerSound.Stop();
					}
			});
		}
	}
}
