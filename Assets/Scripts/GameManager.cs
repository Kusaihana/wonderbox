﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager gm;

	public string levelAfterVictory;
	public string levelAfterGameOver;

	GameObject _player;

	void Awake () {
		if (gm == null)
			gm = this.GetComponent<GameManager>();

		setupDefaults();
	}

	void setupDefaults() {

		if (_player == null)
			_player = GameObject.FindGameObjectWithTag("Player");

		if (_player==null)
			Debug.LogError("Player not found in Game Manager");

		if (levelAfterVictory=="") {
			Debug.LogWarning("levelAfterVictory not specified, defaulted to current level");
			levelAfterVictory = Application.loadedLevelName;
		}

		if (levelAfterGameOver=="") {
			Debug.LogWarning("levelAfterGameOver not specified, defaulted to current level");
			levelAfterGameOver = Application.loadedLevelName;
		}
	}

	public void ResetGame() {
		Debug.Log("Reseting...");
		Application.LoadLevel (levelAfterGameOver);

	}

	public void LevelCompete() {
		StartCoroutine(LoadNextLevel());
	}

	IEnumerator LoadNextLevel() {
		yield return new WaitForSeconds(3.5f); 
		Application.LoadLevel (levelAfterVictory);
	}
}
