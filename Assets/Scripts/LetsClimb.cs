﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetsClimb : MonoBehaviour {

	public GameObject player;

	public AudioSource climbsound;

	//enter the trigger
	public void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject == player)
		{
			Debug.Log("can climb");
			player.GetComponent<PlayerController>().canClimb = true;
			//climbsound.Play();
			//climbsound.loop = true;
		}
	}

	//exit the trigger     
	public void OnTriggerExit2D(Collider2D col)
	{
		if(col.gameObject == player)
		{
			player.GetComponent<PlayerController>().canClimb = false;
			//climbsound.Stop();
		}
	}
}
