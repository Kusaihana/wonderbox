﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

	private PlayerController _emmy;

	public GameObject MudIcon, PaintIcon, JarIcon, BottleIcon, RockIcon;

	void Start(){
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		JarIcon.SetActive(_emmy.HasJam);
		MudIcon.SetActive(_emmy.HasMud);
		PaintIcon.SetActive(_emmy.HasPaintCan);
		BottleIcon.SetActive(_emmy.HasWaterBottle);
		RockIcon.SetActive(_emmy.HasRock);
	}
}
