﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;

public class Lamp : MonoBehaviour {

	public GameObject BrokenLamp;
	public AudioSource BreakeSound;

	private bool _isPlayed;

	void OnCollisionEnter2D (Collision2D other) {

		if (other.gameObject.tag == "Player")
		{
			gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
			gameObject.GetComponent<Rigidbody2D>().gravityScale = 10;
			other.gameObject.GetComponent<PlayerController>().FreezeMotion();
			Observable.Timer(TimeSpan.FromSeconds(2.2f)).Subscribe(x => {
				if(!_isPlayed){
					BreakeSound.Play();
					_isPlayed = true;
				}
				gameObject.GetComponent<Renderer>().enabled = false;
				BrokenLamp.SetActive(true);
				gameObject.GetComponent<PolygonCollider2D>().enabled = false;
				gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
				gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
				gameObject.GetComponent<Rigidbody2D>().collisionDetectionMode  = CollisionDetectionMode2D.Continuous;//RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;});
				other.gameObject.GetComponent<PlayerController>().UnFreezeMotion();
			});
		}
	}

}
