﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class Dog : MonoBehaviour {

	public GameObject PaintCover1, PaintCover2;
	public GameObject DogAngry;

	public AudioSource DangerSound;
	public AudioSource JamSound;

	//public Animator Anim; 
	public List<AudioSource> DogSounds;

	private GameObject _emmy;

	public int CoverCount = 0;

	void Start(){
		if (_emmy == null)
			_emmy = GameObject.FindGameObjectWithTag("Player");
	}

	void Update(){
		if(CoverCount == 2){
			gameObject.GetComponent<Scare>().PuzzleSolved = true;
			DangerSound.Stop();
			//Anim.speed = 0;
			DogAngry.SetActive(false);
			gameObject.GetComponent<Renderer>().enabled = true;
		}else if(_emmy.transform.position.x > -89 ){
			DogAngry.SetActive(true);
			gameObject.GetComponent<Renderer>().enabled = false;
		}
	}
		
	void OnMouseDown(){
		if(_emmy.GetComponent<PlayerController>().HasJam){
			_emmy.GetComponent<PlayerController>().animator.SetTrigger("Throw");
			JamSound.Play();
			Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(x => {
				//Color myColor = new Color(121.0f/255, 76.0f/255, 19.0f/255);
				//gameObject.GetComponent<SpriteRenderer>().color = myColor;
				PaintCover1.SetActive(true);
				CoverCount++;
				//_emmy.GetComponent<PlayerController>().NextLevelCanvas.gameObject.SetActive(true);
				_emmy.GetComponent<PlayerController>().InventoryText.text = "Inventory: ";
				_emmy.GetComponent<PlayerController>().HasJam = false;
				DogSounds[0].Play();
			});
		}
	}
}
